/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.binding;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = "inproceedings")
public class InProceedings {

	private String key;

	private String mdate;

	private String publtype;

	private List<Author> author;

	private List<Editor> editor;

	private List<Title> title;

	private List<BookTitle> booktitle;

	private List<Pages> pages;

	private List<Year> year;

	private List<Address> address;

	private List<Journal> journal;

	private List<Volume> volume;

	private List<Month> month;

	private List<URL> url;

	private List<EE> ee;

	private List<CDRom> cdrom;

	private List<Cite> cite;

	private List<Publisher> publisher;

	private List<Note> note;

	private List<CrossRef> crossRef;

	private List<ISBN> isbn;

	private List<Series> series;

	private List<School> school;

	private List<Chapter> chapter;

	@XmlAttribute (name = "key", required = true)
	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	@XmlAttribute (name = "mdate")
	public String getMdate () {
		return mdate;
	}

	public void setMdate ( final String mdate ) {
		this.mdate = mdate;
	}

	@XmlAttribute (name = "publtype")
	public String getPubltype () {
		return publtype;
	}

	public void setPubltype ( final String publtype ) {
		this.publtype = publtype;
	}

	@XmlElement (name = "author")
	public List<Author> getAuthor () {
		return author;
	}

	public void setAuthor ( final List<Author> author ) {
		this.author = author;
	}

	@XmlElement (name = "editor")
	public List<Editor> getEditor () {
		return editor;
	}

	public void setEditor ( final List<Editor> editor ) {
		this.editor = editor;
	}

	@XmlElement (name = "title")
	public List<Title> getTitle () {
		return title;
	}

	public void setTitle ( final List<Title> title ) {
		this.title = title;
	}

	@XmlElement (name = "booktitle")
	public List<BookTitle> getBooktitle () {
		return booktitle;
	}

	public void setBooktitle ( final List<BookTitle> booktitle ) {
		this.booktitle = booktitle;
	}

	@XmlElement (name = "pages")
	public List<Pages> getPages () {
		return pages;
	}

	public void setPages ( final List<Pages> pages ) {
		this.pages = pages;
	}

	@XmlElement (name = "year")
	public List<Year> getYear () {
		return year;
	}

	public void setYear ( final List<Year> year ) {
		this.year = year;
	}

	@XmlElement (name = "address")
	public List<Address> getAddress () {
		return address;
	}

	public void setAddress ( final List<Address> address ) {
		this.address = address;
	}

	@XmlElement (name = "journal")
	public List<Journal> getJournal () {
		return journal;
	}

	public void setJournal ( final List<Journal> journal ) {
		this.journal = journal;
	}

	@XmlElement (name = "volume")
	public List<Volume> getVolume () {
		return volume;
	}

	public void setVolume ( final List<Volume> volume ) {
		this.volume = volume;
	}

	@XmlElement (name = "month")
	public List<Month> getMonth () {
		return month;
	}

	public void setMonth ( final List<Month> month ) {
		this.month = month;
	}

	@XmlElement (name = "url")
	public List<URL> getUrl () {
		return url;
	}

	public void setUrl ( final List<URL> url ) {
		this.url = url;
	}

	@XmlElement (name = "ee")
	public List<EE> getEe () {
		return ee;
	}

	public void setEe ( final List<EE> ee ) {
		this.ee = ee;
	}

	@XmlElement (name = "cdrom")
	public List<CDRom> getCdrom () {
		return cdrom;
	}

	public void setCdrom ( final List<CDRom> cdrom ) {
		this.cdrom = cdrom;
	}

	@XmlElement (name = "cite")
	public List<Cite> getCite () {
		return cite;
	}

	public void setCite ( final List<Cite> cite ) {
		this.cite = cite;
	}

	@XmlElement (name = "publisher")
	public List<Publisher> getPublisher () {
		return publisher;
	}

	public void setPublisher ( final List<Publisher> publisher ) {
		this.publisher = publisher;
	}

	@XmlElement (name = "note")
	public List<Note> getNote () {
		return note;
	}

	public void setNote ( final List<Note> note ) {
		this.note = note;
	}

	@XmlElement (name = "crossref")
	public List<CrossRef> getCrossRef () {
		return crossRef;
	}

	public void setCrossRef ( final List<CrossRef> crossRef ) {
		this.crossRef = crossRef;
	}

	@XmlElement (name = "isbn")
	public List<ISBN> getIsbn () {
		return isbn;
	}

	public void setIsbn ( final List<ISBN> isbn ) {
		this.isbn = isbn;
	}

	@XmlElement (name = "series")
	public List<Series> getSeries () {
		return series;
	}

	public void setSeries ( final List<Series> series ) {
		this.series = series;
	}

	@XmlElement (name = "school")
	public List<School> getSchool () {
		return school;
	}

	public void setSchool ( final List<School> school ) {
		this.school = school;
	}

	@XmlElement (name = "chapter")
	public List<Chapter> getChapter () {
		return chapter;
	}

	public void setChapter ( final List<Chapter> chapter ) {
		this.chapter = chapter;
	}

	@Override
	public String toString () {
		return "InProceedings{" +
				"\nkey='" + key + '\'' +
				"\n, mdate='" + mdate + '\'' +
				"\n, publtype='" + publtype + '\'' +
				"\n, author=" + author +
				"\n, editor=" + editor +
				"\n, title=" + title +
				"\n, booktitle=" + booktitle +
				"\n, pages=" + pages +
				"\n, year=" + year +
				"\n, address=" + address +
				"\n, journal=" + journal +
				"\n, volume=" + volume +
				"\n, month=" + month +
				"\n, url=" + url +
				"\n, ee=" + ee +
				"\n, cdrom=" + cdrom +
				"\n, cite=" + cite +
				"\n, publisher=" + publisher +
				"\n, note=" + note +
				"\n, crossRef=" + crossRef +
				"\n, isbn=" + isbn +
				"\n, series=" + series +
				"\n, school=" + school +
				"\n, chapter=" + chapter +
				'}';
	}
}
