/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.ConferenceClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.restful.Conference;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ConferenceProcessor extends DBLPProcessor {

	public ConferenceProcessor ( final String path, final long startYear, final long endYear ) {
		super ( path, startYear, endYear );
	}

	@Override
	public void run () {
		if ( unmarshallerInProc == null ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to execute Conference extraction. File is not readable." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 );
		builder.totalProgress ( 2 );

		boolean success = extractConferencesAcronym ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Conference properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );
		builder.totalProgress ( 2 );

		success = dblpWebAccess ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "DBLP access failed, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Extraction fully completed. " )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	private boolean dblpWebAccess () {
		boolean success = true;
		builder
				.text ( "Extracting meta-data from <a href='http://www.dblp.org/db' target='_blank'>dblp.org/db</a>, fully synchronizing new entries. This operation performs all modifications in a single transaction." );
		builder.subProgress ( 1 ).totalSubProgress ( conferencesAcronyms.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		WebPageClient webClient = WebPageClient.create ( APIClient.REST_API );
		ConferenceClient conferenceClient = ConferenceClient.create ( APIClient.REST_API, sessionId );
		int count = 0;

		for ( String acronym : conferencesAcronyms ) {
			WebPage dblpPage = new WebPage ();
			String url = DBLPUtils.DBLP_DB + "conf/" + acronym + "/index.html";
			dblpPage.setUrl ( url );
			dblpPage = webClient.getWebPage ( dblpPage );
			if ( !webClient.isValidWebPage ( dblpPage ) ) {
				continue;
			}

			Conference conference = new Conference ();
			conference.setUrl ( url );
			conference.setAcronym ( acronym );
			conference.setKey ( "conf/" + acronym );
			conference = parseConferenceFromDBLP ( dblpPage.getContent (), conference );
			String confId = conferenceClient.post ( conference );
			conference.setId ( confId );

			++count;
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
		}
		session.commitSession ();
		return success;
	}

	public Conference parseConferenceFromDBLP ( final String content, final Conference conference ) {
		Document doc = Jsoup.parse ( content );
		String acronym = doc.title ();
		acronym = acronym.replace ( "dblp: ", "" );

		String title = doc.select ( "h1" ).text ();
		int startAcronym = title.indexOf ( " (" + acronym + ")" );
		if ( startAcronym != -1 ) {
			title = title.substring ( 0, startAcronym );
			conference.setTitle ( title );
			conference.setAcronym ( acronym );
		} else {
			conference.setTitle ( title );
		}

		return conference;
	}


}
