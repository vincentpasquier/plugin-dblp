/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.PeopleClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.Normalizer;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ProceedingCommitteeProcessor extends DBLPProcessor {

	private final Multimap<String, String> proceedingsCommitteeMappingConferenceKey =
			HashMultimap.create ();

	private final Multimap<String, String> proceedingsCommitteeIdMappingConferenceKey =
			HashMultimap.create ();

	int countProceedingsCommittee = 0;

	public ProceedingCommitteeProcessor (
			final String path,
			final long startYear,
			final long endYear ) {
		super ( path, startYear, endYear );
	}

	@Override
	public void run () {
		if ( unmarshallerInProc == null ) {
			PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to execute Author extraction. File is not readable" );
			return;
		}

		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 ).totalProgress ( 4 );

		boolean success = extractConferencesAcronym ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Conference properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 ).totalProgress ( 4 );

		success = extractProceedingsCommittees ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Proceedings committee, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 3 ).totalProgress ( 4 );

		success = commitProceedingsCommittee ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to commit Proceedings committee, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 4 ).totalProgress ( 4 );

		success = mergeProceedingsCommitteeConferences ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to merge Proceedings committee and Conferences, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Extraction fully completed. " )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}

	}

	private boolean extractProceedingsCommittees () {
		boolean success = true;
		builder
				.text ( "Extracting meta-data from <a href='http://www.dblp.org/db' target='_blank'>dblp.org/db</a>, fully synchronizing new entries. This operation performs all modifications in a single transaction." )
				.subProgress ( 1 )
				.totalSubProgress ( conferencesAcronyms.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		int count = 0;

		WebPageClient webClient = WebPageClient.create ( APIClient.REST_API );
		for ( String acronym : conferencesAcronyms ) {
			WebPage dblpPage = new WebPage ();
			String url = DBLPUtils.DBLP_DB + "conf/" + acronym + "/index.html";
			dblpPage.setUrl ( url );
			dblpPage = webClient.getWebPage ( dblpPage );
			if ( !webClient.isValidWebPage ( dblpPage ) ) {
				continue;
			}
			Set<String> names = parseDBLPProceedingsCommittees ( dblpPage.getContent () );
			int before = proceedingsCommitteeMappingConferenceKey.keySet ().size ();
			for ( String name : names ) {
				proceedingsCommitteeMappingConferenceKey.put ( name, "conf/" + acronym );
			}
			int after = proceedingsCommitteeMappingConferenceKey.keySet ().size ();
			countProceedingsCommittee += after - before;
			++count;
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		return success;
	}

	private Set<String> parseDBLPProceedingsCommittees ( final String content ) {
		Set<String> names = new HashSet<> ();
		Document doc = Jsoup.parse ( content );
		for ( Element venues : doc.select ( ".publ-list" ).select ( "li" ) ) {
			for ( Element venue : venues.select ( "li[id^=conf]" ) ) {
				String key = venue.attr ( "id" );
				if ( key != null ) {
					Element data = venues.select ( "div.data" ).first ();
					Elements proceedingsCommittees =
							data.select ( "a[href^=http://www.informatik.uni-trier.de/~ley/pers]" );
					for ( Element element : proceedingsCommittees ) {
						String name = element.text ();
						name = Normalizer.normalize ( name, Normalizer.Form.NFD );
						name = name.replaceAll ( "\\p{M}", "" );
						names.add ( name );
					}
				}
			}
		}
		return names;
	}

	private boolean commitProceedingsCommittee () {
		boolean success = true;
		builder
				.text ( "Commit all proceedings committees and synchronizes with existing triple in database. This operation performs all modifications in single or multiple transaction depending on size." )
				.subProgress ( 0 )
				.totalSubProgress ( countProceedingsCommittee );
		PluginInterface.publishPluginStatus ( builder.build () );

		int count = 0;

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
		for ( String proceedingsCommittee : proceedingsCommitteeMappingConferenceKey.keySet () ) {
			Person person = new Person ();
			person.setName ( proceedingsCommittee );
			String personId = peopleClient.post ( person );
			proceedingsCommitteeIdMappingConferenceKey.putAll (
					personId,
					proceedingsCommitteeMappingConferenceKey.get ( proceedingsCommittee ) );

			++count;
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			if ( ( count % 20000 ) == 0 ) {
				session.commitSession ();
				sessionId = session.createSession ();
				peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();

		return success;
	}

	private boolean mergeProceedingsCommitteeConferences () {
		boolean success = true;
		builder
				.text ( "Merges all proceedings committees with their conferences and synchronizes with existing triple in database. This operation performs all modifications in single or multiple transaction depending on size." )
				.subProgress ( 0 )
				.totalSubProgress ( proceedingsCommitteeIdMappingConferenceKey.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );

		int count = 0;

		for ( String pcId : proceedingsCommitteeIdMappingConferenceKey.keySet () ) {
			for ( String conferenceKey : proceedingsCommitteeIdMappingConferenceKey.get ( pcId ) ) {
				peopleClient.addConference ( pcId, conferenceKey );

				++count;
				if ( ( count % 100 ) == 0 ) {
					builder.subProgress ( count );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
				if ( ( count % 20000 ) == 0 ) {
					session.commitSession ();
					sessionId = session.createSession ();
					peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
				}
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();


		return success;
	}

}
