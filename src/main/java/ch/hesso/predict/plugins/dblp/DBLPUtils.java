/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp;

import ch.hesso.predict.plugins.dblp.binding.*;
import ch.hesso.predict.restful.Doi;
import ch.hesso.predict.restful.Publication;
import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public final class DBLPUtils {

	/**
	 *
	 */
	public static final Pattern CONF_PATTERN =
			Pattern.compile ( "(([A-z0-9]*/([A-z0-9\\-\\+]*))/[A-z0-9\\-\\+]*)" );

	/**
	 *
	 */
	public static final String DBLP_DB = "http://www.informatik.uni-trier.de/~ley/db/";

	private DBLPUtils () {
		throw new AssertionError ( "This is an utility class, do not try to instanciate it." );
	}

	/**
	 * @param years
	 *
	 * @return
	 */
	public static boolean isYearExtractable (
			final List<Year> years,
			final long startYear,
			final long endYear ) {
		boolean extract = years != null
				&& !years.isEmpty ()
				&& years.get ( 0 ).getContent () != null;
		if ( extract ) {
			int year = Integer.parseInt ( years.get ( 0 ).getContent () );
			extract = year >= startYear && year <= endYear;
		}
		return extract;
	}

	/**
	 * @param years
	 *
	 * @return
	 */
	public static int extractYear (
			final List<Year> years,
			final long startYear,
			final long endYear ) {
		int year = 0;
		if ( isYearExtractable ( years, startYear, endYear ) ) {
			String yearText = years.get ( 0 ).getContent ();
			year = Integer.parseInt ( yearText );
		}
		return year;
	}

	/**
	 * @param key
	 *
	 * @return
	 */
	public static boolean isConference ( final String key ) {
		Matcher matcher = CONF_PATTERN.matcher ( key );
		return matcher.find ()
				&& matcher.group ( 2 ).startsWith ( "conf" );
	}

	/**
	 * @param key
	 *
	 * @return
	 */
	public static String extractAcronym ( final String key ) {
		Matcher matcher = CONF_PATTERN.matcher ( key );
		String acronym = "";
		if ( matcher.find () ) {
			acronym = matcher.group ( 3 );
		}
		return acronym;
	}

	/**
	 * @param crossRefs
	 *
	 * @return
	 */
	public static boolean hasVenue ( final List<CrossRef> crossRefs ) {
		return crossRefs != null
				&& !crossRefs.isEmpty ()
				&& crossRefs.get ( 0 ) != null
				&& !crossRefs.get ( 0 ).getContent ().equals ( "" );
	}

	/**
	 * @param crossRefs
	 *
	 * @return
	 */
	public static String extractVenueKey ( final List<CrossRef> crossRefs ) {
		try {
			return crossRefs.get ( 0 ).getContent ();
		} catch ( Exception e ) {
			return "";
		}
	}

	public static boolean hasURLDOI ( final List<EE> ees ) {
		return ees != null
				&& !ees.isEmpty ()
				&& ees.get ( 0 ).getContent () != null;
	}

	/**
	 * @param ees
	 *
	 * @return
	 */
	public static String extractURLDOI ( final List<EE> ees ) {
		String urlDoi = "";
		if ( hasURLDOI ( ees ) ) {
			urlDoi = ees.get ( 0 ).getContent ();
		}
		return urlDoi;
	}

	/**
	 * @param booktitle
	 *
	 * @return
	 */
	public static boolean hasBookTitle ( final List<BookTitle> booktitle ) {
		return booktitle != null
				&& !booktitle.isEmpty ()
				&& booktitle.get ( 0 ).getContent () != null;
	}

	/**
	 * @param title
	 *
	 * @return
	 */
	public static boolean hasTitle ( final List<Title> title ) {
		return title != null
				&& !title.isEmpty ()
				&& title.get ( 0 ).getContent () != null;
	}

	/**
	 * @param bookTitles
	 *
	 * @return
	 */
	public static String extractBookTitle ( final List<BookTitle> bookTitles ) {
		String title = "";
		if ( hasBookTitle ( bookTitles ) ) {
			title = bookTitles.get ( 0 ).getContent ();
		}
		return title;
	}

	/**
	 * @param titles
	 *
	 * @return
	 */
	public static String extractTitle ( final List<Title> titles ) {
		String title = "";
		if ( hasTitle ( titles ) ) {
			title = titles.get ( 0 ).getContent ();
		}
		return title;
	}

	/**
	 * @param publisher
	 *
	 * @return
	 */
	public static boolean hasPublisher ( final List<Publisher> publisher ) {
		return publisher != null
				&& !publisher.isEmpty ()
				&& publisher.get ( 0 ).getContent () != null;
	}

	/**
	 * @param editor
	 *
	 * @return
	 */
	public static boolean hasEditor ( final List<Editor> editor ) {
		return editor != null
				&& !editor.isEmpty ()
				&& editor.get ( 0 ).getContent () != null;
	}

	/**
	 * @param pages
	 *
	 * @return
	 */
	public static boolean hasPages ( final List<Pages> pages ) {
		return pages != null
				&& !pages.isEmpty ()
				&& pages.get ( 0 ).getContent () != null;
	}

	/**
	 * @param publication
	 * @param editor
	 */
	public static void fillEditor ( final Publication publication, final List<Editor> editor ) {
		if ( DBLPUtils.hasEditor ( editor ) ) {
			publication.setEditor ( editor.get ( 0 ).getContent () );
		}
	}

	/**
	 * @param publication
	 * @param pages
	 */
	public static void fillPages ( final Publication publication, final List<Pages> pages ) {
		if ( DBLPUtils.hasPages ( pages ) ) {
			publication.setPages ( pages.get ( 0 ).getContent () );
		}
	}

	/**
	 * @param publication
	 * @param publisher
	 */
	public static void fillPublisher ( final Publication publication, final List<Publisher> publisher ) {
		if ( DBLPUtils.hasPublisher ( publisher ) ) {
			publication.setPublisher ( publisher.get ( 0 ).getContent () );
		}
	}

	/**
	 * @param publication
	 * @param ip
	 *
	 * @return
	 */
	public static boolean fillTitleBookTitle ( final Publication publication, final InProceedings ip ) {
		boolean success = DBLPUtils.hasTitle ( ip.getTitle () );
		if ( success ) {
			publication.setTitle ( DBLPUtils.extractTitle ( ip.getTitle () ) );
		}
		if ( DBLPUtils.hasBookTitle ( ip.getBooktitle () ) ) {
			publication.setBooktitle ( DBLPUtils.extractBookTitle ( ip.getBooktitle () ) );
		}
		return success;
	}

	/**
	 * @param publication
	 * @param ip
	 *
	 * @return
	 */
	public static boolean fillUrlDoi ( final Publication publication, final InProceedings ip ) {
		boolean success = DBLPUtils.hasURLDOI ( ip.getEe () );
		if ( success ) {
			String urlDoi = DBLPUtils.extractURLDOI ( ip.getEe () );
			if ( Doi.DOI_PATTERN.matcher ( urlDoi ).find () ) {
				publication.setDoi ( urlDoi );
			} else {
				publication.setUrl ( urlDoi );
			}
		}
		return success;
	}

	/**
	 * @param author
	 *
	 * @return
	 */
	public static boolean hasAuthors ( final List<Author> author ) {
		return author != null
				&& !author.isEmpty ();
	}

	/**
	 * @param authors
	 *
	 * @return
	 */
	public static Set<String> extractAuthorsName ( final List<Author> authors ) {
		Set<String> toReturn = new HashSet<> ();
		for ( Author author : authors ) {
			toReturn.add ( author.getContent () );
		}
		return toReturn;
	}

	/**
	 * @param title
	 *
	 * @return
	 */
	public static List<Language> detectTitleLanguage ( final String title ) {
		List<Language> toReturn = new ArrayList<> ();
		try {
			Detector detector = DetectorFactory.create ();
			detector.append ( title );
			toReturn = detector.getProbabilities ();
		} catch ( LangDetectException e ) {
		}
		return toReturn;
	}

	public static boolean containsLanguageAboveThreshold (
			final List<Language> languages,
			final String target,
			final double threshold ) {
		boolean contains = false;
		for ( Language language : languages ) {
			contains |= language.lang.equals ( target ) && language.prob > threshold;
		}
		return contains;
	}
}
