/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.PeopleClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.restful.Person;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AuthorProcessor extends DBLPProcessor {

	public AuthorProcessor ( final String path,
													 final long startYear,
													 final long endYear ) {
		super ( path, startYear, endYear );
	}

	private final Multimap<String, String> authorNameMappingPublicationKey =
			HashMultimap.create ();

	private final Multimap<String, String> authorIdMappingPublicationKey =
			HashMultimap.create ();

	private int authorCount = 0;

	@Override
	public void run () {
		if ( unmarshallerInProc == null ) {
			PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to execute Author extraction. File is not readable" );
			return;
		}

		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 );
		builder.totalProgress ( 3 );

		boolean success = extractAuthors ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );

		success = commitAuthors ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to commit Authors properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 3 );

		success = mergeAuthors ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to merge Authors and Publications properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Extraction fully completed. " )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	private boolean extractAuthors () {
		boolean success = true;
		builder.text ( "Extracting DBLP dataset authors in a single pass. Gets all name and corresponding publications key." );
		PluginInterface.publishPluginStatus ( builder.build () );

		try {
			while ( unmarshallerInProc.hasNext () ) {
				InProceedings ip = unmarshallerInProc.next ();
				String publicationKey = ip.getKey ();
				if ( DBLPUtils.hasAuthors ( ip.getAuthor () )
						&& DBLPUtils.isYearExtractable ( ip.getYear (), startYear, endYear ) ) {
					Set<String> peopleName = DBLPUtils.extractAuthorsName ( ip.getAuthor () );
					int before = authorNameMappingPublicationKey.keySet ().size ();
					for ( String personName : peopleName ) {
						authorNameMappingPublicationKey.put ( personName, publicationKey );
					}
					int after = authorNameMappingPublicationKey.keySet ().size ();
					authorCount += after - before;
					builder.subProgress ( authorCount );
				}
			}
		} catch ( XMLStreamException | JAXBException e ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Stream error while reading DBLP file, please check file integrity." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			success = false;
		}

		return success;
	}

	private boolean commitAuthors () {
		boolean success = true;
		builder
				.text ( "Commit all authors and synchronizes with existing triple in database. This operation performs all modifications in single or multiple transaction depending on size." )
				.subProgress ( 0 )
				.totalSubProgress ( authorCount );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );

		int count = 0;

		for ( String author : authorNameMappingPublicationKey.keySet () ) {
			Person person = new Person ();
			person.setName ( author );
			String personId = peopleClient.post ( person );
			authorIdMappingPublicationKey.putAll ( personId, authorNameMappingPublicationKey.get ( author ) );

			++count;
			if ( ( count % 100 ) == 0 ) {
				builder.subProgress ( count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
			if ( ( count % 20000 ) == 0 ) {
				session.commitSession ();
				sessionId = session.createSession ();
				peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();

		return success;
	}

	private boolean mergeAuthors () {
		boolean success = true;
		builder
				.text ( "Merges all authors with their publications and synchronizes with existing triple in database. This operation performs all modifications in single or multiple transaction depending on size." )
				.subProgress ( 0 )
				.totalSubProgress ( authorIdMappingPublicationKey.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PeopleClient peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );

		int count = 0;

		for ( String authorId : authorIdMappingPublicationKey.keySet () ) {
			for ( String publicationKey : authorIdMappingPublicationKey.get ( authorId ) ) {
				peopleClient.addPublication ( authorId, publicationKey );

				++count;
				if ( ( count % 100 ) == 0 ) {
					builder.subProgress ( count );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
				if ( ( count % 20000 ) == 0 ) {
					session.commitSession ();
					sessionId = session.createSession ();
					peopleClient = PeopleClient.create ( APIClient.REST_API, sessionId );
				}
			}
		}
		builder.subProgress ( count );
		PluginInterface.publishPluginStatus ( builder.build () );
		session.commitSession ();

		return success;
	}

}
