/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.plugins.dblp.binding.*;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class DBLPProcessor {

	protected final Set<String> conferencesAcronyms = new HashSet<> ();

	protected FileInputStream fis;

	protected PartialUnmarshaller<InProceedings> unmarshallerInProc;

	protected long startYear;

	protected long endYear;

	protected PluginInterface.PluginStatus.Builder builder;

	public DBLPProcessor (
			final String path,
			final long startYear,
			final long endYear ) {
		try {
			fis = new FileInputStream ( new File ( path ) );
			unmarshallerInProc = new PartialUnmarshaller<> ( fis, InProceedings.class );
			this.startYear = startYear;
			this.endYear = endYear;
			builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		} catch ( FileNotFoundException e ) {
			// Sending error
			PluginInterface.PluginStatus.Builder builder =
					PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR );
			builder.text ( "Unable to open DBLP dataset file. Make sure it is correctly set." );
			PluginInterface.publishPluginStatus ( builder.build () );
		} catch ( JAXBException | XMLStreamException e ) {
			// Sending error
			PluginInterface.PluginStatus.Builder builder =
					PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR );
			builder.text ( "Error while reading file. It appears the file is not correctly formed." );
			PluginInterface.publishPluginStatus ( builder.build () );
		}
	}

	protected boolean extractConferencesAcronym () {
		boolean success = true;
		builder.text ( "Extracting DBLP dataset conferences key name." );
		PluginInterface.publishPluginStatus ( builder.build () );
		try {
			while ( unmarshallerInProc.hasNext () ) {
				InProceedings ip = unmarshallerInProc.next ();
				String key = ip.getKey ();
				if ( !DBLPUtils.hasVenue ( ip.getCrossRef () ) ) {
					continue;
				}

				if ( !DBLPUtils.isYearExtractable ( ip.getYear (), startYear, endYear ) ) {
					continue;
				}

				if ( DBLPUtils.isConference ( key ) ) {
					conferencesAcronyms.add ( DBLPUtils.extractAcronym ( key ) );
				}
			}
		} catch ( XMLStreamException | JAXBException e ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Stream error while reading DBLP file, please check file integrity." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			success = false;
		}
		return success;
	}

	public abstract void run ();
}
