/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.client.resources.VenueClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.dblp.DBLPPluginInterface;
import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.restful.Venue;
import ch.hesso.predict.restful.WebPage;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class VenueProcessor extends DBLPProcessor {

	public VenueProcessor ( final String path, final long startYear, final long endYear ) {
		super ( path, startYear, endYear );
	}

	private final Multimap<String, String> conferenceKeyMappingVenueId
			= HashMultimap.create ();

	private int venueCount = 0;

	@Override
	public void run () {
		if ( unmarshallerInProc == null ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to execute Venue extraction. File is not readable" )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}

		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 );
		builder.totalProgress ( 3 );

		boolean success = extractConferencesAcronym ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Venue properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );
		builder.totalProgress ( 3 );

		success = dblpWebAccess ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "DBLP access failed, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
		builder.progress ( 3 );
		builder.totalProgress ( 3 );

		success = sesameMerge ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Failed to merge conferences and venues, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Extraction fully completed. " )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	private boolean dblpWebAccess () {
		boolean success = true;
		builder
				.text ( "Extracting meta-data from <a href='http://www.dblp.org/db' target='_blank'>dblp.org/db</a>, fully synchronizing new entries. This operation performs all modifications in a single transaction." );
		builder.subProgress ( 0 ).totalSubProgress ( 0 );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		WebPageClient webClient = WebPageClient.create ( APIClient.REST_API );
		VenueClient venueClient = VenueClient.create ( APIClient.REST_API, sessionId );
		int count = 1;

		for ( String acronym : conferencesAcronyms ) {
			WebPage dblpPage = new WebPage ();
			String url = DBLPUtils.DBLP_DB + "conf/" + acronym + "/index.html";
			dblpPage.setUrl ( url );
			dblpPage = webClient.getWebPage ( dblpPage );
			if ( !webClient.isValidWebPage ( dblpPage ) ) {
				continue;
			}

			Set<Venue> venues = parseVenueFromDBLP ( dblpPage.getContent () );
			Set<String> committed = new HashSet<> ();
			for ( Venue venue : venues ) {
				String venueId = venueClient.post ( venue );
				venue.setId ( venueId );
				committed.add ( venueId );
				venueCount++;
				count++;
				if ( ( count % 100 ) == 0 ) {
					builder.subProgress ( count );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
			}

			String confKey = "conf/" + acronym;
			conferenceKeyMappingVenueId.putAll ( confKey, committed );
		}
		session.commitSession ();

		return success;
	}

	private boolean sesameMerge () {
		boolean success = true;
		builder
				.text ( "Merging extracted conferences and venues on Sesame triplestore. This operation performs all modifications in a single transaction." );
		builder.subProgress ( 0 ).totalSubProgress ( venueCount );
		PluginInterface.publishPluginStatus ( builder.build () );

		int count = 0;
		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		VenueClient venueClient = VenueClient.create ( APIClient.REST_API, sessionId );
		for ( String conferenceKey : conferenceKeyMappingVenueId.keySet () ) {
			for ( String venueId : conferenceKeyMappingVenueId.get ( conferenceKey ) ) {
				venueClient.addConference ( venueId, conferenceKey );
				builder.subProgress ( ++count );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
		}

		session.commitSession ();
		return success;
	}

	public Set<Venue> parseVenueFromDBLP ( final String content ) {
		Pattern patternYear = Pattern.compile ( "[A-z0-9]*/[A-z0-9\\-\\+]*/([0-9]{4}|[0-9]{2})" );
		Matcher matcher;
		Document doc = Jsoup.parse ( content );
		Set<Venue> parsedVenues = new HashSet<> ();
		for ( Element venues : doc.select ( ".publ-list" ).select ( "li" ) ) {
			for ( Element venue : venues.select ( "li[id^=conf]" ) ) {
				String key = venue.attr ( "id" );
				if ( key != null ) {
					// it's a venue.
					Venue parsedVenue = new Venue ();
					parsedVenue.setKey ( key );
					matcher = patternYear.matcher ( key );
					if ( matcher.find () ) {
						int year = Integer.parseInt ( matcher.group ( 1 ) );
						if ( year < 100 ) {
							year = 1900 + year;
						}
						if ( year < startYear || year > endYear ) {
							continue;
						}
						parsedVenue.setYear ( year );
					}

					Element data = venues.select ( "div.data" ).first ();
					String venueTitle = data.select ( "span.title" ).text ();
					parsedVenue.setTitle ( venueTitle );

					Set<String> locations = new HashSet<> ();
					// Cast done knowingly, do not panic !
					List<List<CoreLabel>> allLabels =
							DBLPPluginInterface.CLASSIFIER.classify ( venueTitle );
					for ( List<CoreLabel> labels : allLabels ) {
						for ( CoreLabel label : labels ) {
							if ( label.get ( CoreAnnotations.AnswerAnnotation.class ).equals ( "LOCATION" ) ) {
								locations.add ( label.word () );
							}
						}
					}
					parsedVenue.setLocation ( locations );
					parsedVenues.add ( parsedVenue );
				}
			}
		}
		return parsedVenues;
	}
}
