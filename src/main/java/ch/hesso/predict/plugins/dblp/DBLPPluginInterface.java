/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.dblp.extractors.*;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.annotations.LongParameter;
import ch.hesso.websocket.annotations.StringParameter;
import ch.hesso.websocket.plugins.PluginInterface;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.NERClassifierCombiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DBLPPluginInterface extends PluginInterface {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( DBLPPluginInterface.class );

	private final Set<APIEntityVisitor> _visitors = new HashSet<> ();

	protected static final String NER;

	protected static final String LANG_PROFILE;

	public static AbstractSequenceClassifier CLASSIFIER;

	static {
		String ner = System.getProperty ( "predict.plugins.dblp.ner" );
		String langProfile = System.getProperty ( "predict.plugins.dblp.lang" );
		if ( ner == null || langProfile == null ) {
			NER = "";
			LANG_PROFILE = "";
			if ( ner == null ) {
				LOG.error ( "NER property not set (predict.plugins.dblp.ner). Please set it before starting." );
			}
			if ( langProfile == null ) {
				LOG.error ( "Language profile property not set (predict.plugins.dblp.lang). Please set it before starting." );
			}
			System.exit ( -1 );
		} else {
			NER = ner;
			LANG_PROFILE = langProfile;
		}
		try {
			CLASSIFIER = NERClassifierCombiner.loadClassifierFromPath ( NER );
			DetectorFactory.loadProfile ( LANG_PROFILE );
		} catch ( FileNotFoundException | LangDetectException e ) {
		}
	}

	@StringParameter (
			value = "dblp.xml",
			description = "File location of the DBLP dataset. Make sure it is available to the plugin before running any methods."
	)
	private String dblpXMLFile;

	@LongParameter (
			value = 1900,
			description = "The start year at which to synchronize existing data with (inclusive)."
	)
	private long startYear;

	@LongParameter (
			value = 2015,
			description = "The end year at which to synchronize existing data with (inclusive)."
	)
	private long endYear;

	@CallableMethod (
			value = "Extract conferences",
			description = "Perform a single pass to read the whole dataset. Extract all existing conferences, crawl <a href='www.dblp.org/db' target='_blank'>dblp.org/db</a> and synchronizes Sesame triplestore."
	)
	public void extractConferences () {
		DBLPProcessor processor = new ConferenceProcessor ( dblpXMLFile, startYear, endYear );
		processor.run ();
	}

	@CallableMethod (
			value = "Extract venues",
			description = "Perform a single pass to read the whole dataset. Extract all existing venues, crawl <a href='www.dblp.org/db' target='_blank'>dblp.org/db</a> and synchronizes Sesame triplestore."
	)
	public void extractVenues () {
		DBLPProcessor processor = new VenueProcessor ( dblpXMLFile, startYear, endYear );
		processor.run ();
	}

	@CallableMethod (
			value = "Extract publications",
			description = "Get all venues and add publication for each with their respective extracted data."
	)
	public void extractPublications () {
		DBLPProcessor processor = new PublicationProcessor ( dblpXMLFile, startYear, endYear );
		processor.run ();
	}

	@CallableMethod (
			value = "Extract authors",
			description = "Extract all authors from dataset and synchronizes with existing publications."
	)
	public void extractAuthors () {
		DBLPProcessor processor = new AuthorProcessor ( dblpXMLFile, startYear, endYear );
		processor.run ();
	}

	@CallableMethod (
			value = "Extract proceedings committee",
			description = "Perform a single pass to read the whole dataset. Extract all existing proceedings committee from <a href='www.dblp.org/db' target='_blank'>dblp.org/db</a> and synchronizes Sesame triplestore."
	)
	public void extractProceedingCommittee () {
		DBLPProcessor processor = new ProceedingCommitteeProcessor ( dblpXMLFile, startYear, endYear );
		processor.run ();
	}

	public DBLPPluginInterface () {
	}

	@Override
	public String name () {
		return "Digital Bibliographic Library Browser (DBLP) Extractor";
	}

	@Override
	public String description () {
		return "This plugin is the <i>DBLP</i> extractor. Works closely with the dataset <a href='http://dblp.uni-trier.de/xml/' target='_blank'>freely available</a>. Every operation synchronizes Sesame triple store.<br>Perform several operations:<ol><li>Extract conferences from <a href='www.dblp.org/db' target='_blank'>dblp.org/db</a> website</li><li>Extract publications from conferences</li><li>Extract authors from publications</li><li>Extract venues for each conferences</li><li>Extract proceedings committees for each conferences</li></ol>";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources = new HashMap<> ();
		//resources.put ( Conference.class, ImmutableSet.of ( HTTPMethod.POST ) );
		return resources;
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return _visitors;
	}

}
