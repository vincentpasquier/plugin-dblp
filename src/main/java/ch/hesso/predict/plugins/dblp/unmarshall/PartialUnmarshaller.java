/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.unmarshall;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static javax.xml.stream.XMLStreamConstants.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PartialUnmarshaller<T> {

	private final XMLStreamReader _reader;

	private final Class<T> _$class;

	private final Unmarshaller _unmarshaller;

	private final String _name;

	public PartialUnmarshaller ( final InputStream stream, final Class<T> $class ) throws XMLStreamException, FactoryConfigurationError, JAXBException {
		_$class = $class;
		_unmarshaller = JAXBContext.newInstance ( $class ).createUnmarshaller ();
		_reader = XMLInputFactory.newInstance ().createXMLStreamReader ( stream );
		_name = $class.getAnnotation ( XmlRootElement.class ).name ();

        /* ignore headers */
		skipElements ( START_DOCUMENT, DTD );
				/* ignore root element */
		_reader.nextTag ();
				/* if there's no tag, ignore root element's end */
		skipElements ( END_ELEMENT );
	}

	public T next () throws XMLStreamException, JAXBException {
		if ( !hasNext () ) {
			throw new NoSuchElementException ();
		}

		while ( _reader.hasNext () ) {
			if ( _reader.isStartElement () && _reader.getLocalName ().equals ( _name ) ) {
				break;
			}
			_reader.next ();
		}

		T value = _unmarshaller.unmarshal ( _reader, _$class ).getValue ();

		skipElements ( CHARACTERS, END_ELEMENT );
		return value;
	}

	public boolean hasNext () throws XMLStreamException {
		return _reader.hasNext ();
	}

	public void close () throws XMLStreamException {
		_reader.close ();
	}

	private void skipElements ( final int... elements ) throws XMLStreamException {
		int eventType = _reader.getEventType ();
		List<Integer> types = new ArrayList<Integer> ( elements.length );
		for ( int index = 0; index < elements.length; index++ ) {
			types.add ( elements[ index ] );
		}
		while ( types.contains ( eventType ) ) {
			eventType = _reader.next ();
		}
	}
}