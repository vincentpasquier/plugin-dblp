/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.PublicationClient;
import ch.hesso.predict.client.resources.SessionClient;
import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.restful.Publication;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.cybozu.labs.langdetect.Language;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.util.Collection;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PublicationProcessor extends DBLPProcessor {

	public PublicationProcessor ( final String path,
																final long startYear,
																final long endYear ) {
		super ( path, startYear, endYear );
	}

	private final Multimap<String, String> venueKeyMappingPublicationId =
			HashMultimap.create ();

	private int countPublications;

	@Override
	public void run () {
		if ( unmarshallerInProc == null ) {
			PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to execute Publication extraction. File is not readable" );
			return;
		}

		builder = PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );
		builder.progress ( 1 );
		builder.totalProgress ( 2 );

		boolean success = extractPublications ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to extract Publications properly, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}
		builder.progress ( 2 );
		builder.totalProgress ( 2 );

		success = mergeVenuePublications ();
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Failed to merge venues and publications, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.DONE )
					.text ( "Extraction fully completed. " )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	public boolean extractPublications () {
		boolean success = true;
		builder.text ( "Extracting DBLP dataset publications and synchronizing with existing triple in database. This operation performs all modifications in single or multiple transaction(s) depending on size" );
		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient session = SessionClient.create ( APIClient.REST_API );
		String sessionId = session.createSession ();
		PublicationClient publicationClient = PublicationClient.create ( APIClient.REST_API, sessionId );
		try {
			while ( unmarshallerInProc.hasNext () ) {
				InProceedings ip = unmarshallerInProc.next ();
				Publication publication = new Publication ();
				if ( extractPublication ( publication, ip ) ) {
					String venueKey = DBLPUtils.extractVenueKey ( ip.getCrossRef () );
					String publicationId = publicationClient.post ( publication );
					venueKeyMappingPublicationId.put ( venueKey, publicationId );
				}
				if ( ( countPublications % 100 ) == 0 ) {
					builder.subProgress ( countPublications );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
				if ( countPublications % 20000 == 0 ) {
					session.commitSession ();
					sessionId = session.createSession ();
					publicationClient = PublicationClient.create ( APIClient.REST_API, sessionId );
				}
			}
			session.commitSession ();
		} catch ( XMLStreamException | JAXBException e ) {
			session.deleteSession ();
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Stream error while reading DBLP file, please check file integrity." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			success = false;
		}

		return success;
	}

	private boolean mergeVenuePublications () {
		boolean success = true;
		builder.text ( "Extracting DBLP dataset publications and synchronizing with existing triple in database. This operation performs all modifications in a single transaction." );
		builder.subProgress ( 0 ).totalSubProgress ( countPublications );

		PluginInterface.publishPluginStatus ( builder.build () );

		SessionClient sessionClient = SessionClient.create ( APIClient.REST_API );
		String sessionId = sessionClient.createSession ();
		PublicationClient publicationClient = PublicationClient.create ( APIClient.REST_API, sessionId );

		int count = 0;

		for ( String venueKey : venueKeyMappingPublicationId.keySet () ) {
			Collection<String> publicationsIds = venueKeyMappingPublicationId.get ( venueKey );
			for ( String publicationId : publicationsIds ) {
				publicationClient.addPublication ( publicationId, venueKey );
				count++;
				if ( ( count % 100 ) == 0 ) {
					builder.subProgress ( count );
					PluginInterface.publishPluginStatus ( builder.build () );
				}
				if ( count % 20000 == 0 ) {
					sessionClient.commitSession ();
					sessionId = sessionClient.createSession ();
					publicationClient = PublicationClient.create ( APIClient.REST_API, sessionId );
				}
			}
		}

		sessionClient.commitSession ();

		return success;
	}

	private boolean extractPublication ( final Publication publication, final InProceedings ip ) {
		boolean success = false;
		for (; ; ) {
			String key = ip.getKey ();
			publication.setKey ( key );
			if ( !DBLPUtils.isConference ( key ) ) {
				break;
			}

			if ( !DBLPUtils.hasVenue ( ip.getCrossRef () ) ) {
				break;
			}

			if ( !DBLPUtils.isYearExtractable ( ip.getYear (), startYear, endYear ) ) {
				break;
			}
			publication.setYear ( DBLPUtils.extractYear ( ip.getYear (), startYear, endYear ) );

			if ( !DBLPUtils.fillUrlDoi ( publication, ip ) ) {
				break;
			}

			if ( !DBLPUtils.fillTitleBookTitle ( publication, ip ) ) {
				break;
			}

			String title = publication.getTitle ();
			List<Language> languages = DBLPUtils.detectTitleLanguage ( title );
			if ( !DBLPUtils.containsLanguageAboveThreshold ( languages, "en", 0.5 )
					&& title.length () > 25 ) {
				break;
			}

			DBLPUtils.fillPublisher ( publication, ip.getPublisher () );
			DBLPUtils.fillPages ( publication, ip.getPages () );
			DBLPUtils.fillEditor ( publication, ip.getEditor () );

			countPublications++;
			success = true;
			break;
		}
		return success;
	}
}
