/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import ch.hesso.predict.plugins.dblp.binding.EE;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestsDataset {

	public static void main ( final String[] args ) throws JAXBException, FileNotFoundException, XMLStreamException {
		datasetDOI ();
		//datasetYears ();
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue ( Map<K, V> map ) {
		List<Map.Entry<K, V>> list = new LinkedList<> ( map.entrySet () );
		Collections.sort ( list, new Comparator<Map.Entry<K, V>> () {
			public int compare ( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
				return ( o1.getValue () ).compareTo ( o2.getValue () );
			}
		} );
		Map<K, V> result = new LinkedHashMap<> ();
		for ( Map.Entry<K, V> entry : list ) {
			result.put ( entry.getKey (), entry.getValue () );
		}
		return result;
	}

	public static void datasetYears () throws FileNotFoundException, JAXBException, XMLStreamException {
		FileInputStream fis = new FileInputStream ( new File ( "/mnt/Damocles/master/dblp/dblp.xml" ) );

		Map<String, Integer> years = new HashMap<> ();
		for ( int i = 1900; i < 2020; i++ ) {
			years.put ( String.valueOf ( i ), 0 );
		}

		PartialUnmarshaller<InProceedings> unmarshallerInProc = new PartialUnmarshaller<> ( fis, InProceedings.class );
		while ( unmarshallerInProc.hasNext () ) {
			InProceedings ip = unmarshallerInProc.next ();
			if ( ip.getYear () != null && ip.getYear ().size () > 0 ) {
				String year = ip.getYear ().get ( 0 ).getContent ();
				years.put ( year, years.get ( year ) + 1 );
			}
		}

		Map<String, Integer> sorted = sortByValue ( years );
		for ( Map.Entry<String, Integer> sort : sorted.entrySet () ) {
			System.out.println ( sort.getValue () + " ~ " + sort.getKey () );
		}
	}

	public static void datasetDOI () throws FileNotFoundException, JAXBException, XMLStreamException {
		FileInputStream fis = new FileInputStream ( new File ( "/home/holmes/Downloads/dblp.xml" ) );
		PartialUnmarshaller<InProceedings> unmarshallerInProc = new PartialUnmarshaller<> ( fis, InProceedings.class );
		Map<String, Integer> doi = new HashMap<> ();
		doi.put ( "jmlr", 0 );
		doi.put ( "doi", 0 );
		doi.put ( "arxiv", 0 );
		doi.put ( "ieeecomp", 0 );
		doi.put ( "ieeexplore", 0 );
		doi.put ( "emis", 0 );
		doi.put ( "cnrs", 0 );
		doi.put ( "iospress", 0 );
		doi.put ( "acm", 0 );
		doi.put ( "aria", 0 );
		doi.put ( "lrec", 0 );
		doi.put ( "pdf", 0 );
		doi.put ( "doc", 0 );
		doi.put ( "strange", 0 );
		doi.put ( "springer", 0 );
		doi.put ( "springerdead", 0 ); // Dead
		doi.put ( "usenix", 0 );
		doi.put ( "aisnet", 0 );
		doi.put ( "aclweb", 0 );
		doi.put ( "computer", 0 );
		doi.put ( "ewic", 0 );
		doi.put ( "vde", 0 );
		doi.put ( "aaai", 0 );
		doi.put ( "easychair", 0 );
		doi.put ( "mundcomp", 0 ); // DE
		doi.put ( "espacedev", 0 );
		doi.put ( "eg", 0 );
		doi.put ( "dagstuhl", 0 );
		doi.put ( "imaging", 0 );
		doi.put ( "zpid", 0 );
		doi.put ( "infosecon", 0 );
		doi.put ( "w3", 0 );
		doi.put ( "nasa", 0 );
		doi.put ( "isca", 0 );
		doi.put ( "stanford", 0 );
		doi.put ( "interspeech", 0 );
		doi.put ( "geoinfo", 0 );
		doi.put ( "unipd", 0 );
		doi.put ( "www2003", 0 );
		doi.put ( "ugr", 0 );
		doi.put ( "hal", 0 );
		doi.put ( "actapress", 0 );
		doi.put ( "ssrn", 0 );
		doi.put ( "stringology", 0 );
		doi.put ( "liu", 0 );
		doi.put ( "icwsm", 0 );
		doi.put ( "bioinformatics", 0 );
		doi.put ( "roboticsproceedings", 0 );
		doi.put ( "icml", 0 );
		doi.put ( "polytechnique", 0 );
		doi.put ( "rostock", 0 );
		doi.put ( "graphicsinterface", 0 );
		doi.put ( "nips", 0 );
		doi.put ( "nii", 0 );
		doi.put ( "internetsociety", 0 ); // Page, pdf link within
		doi.put ( "hdl", 0 ); // Redirect
		doi.put ( "urn", 0 ); // Redirect
		doi.put ( "dmtcs", 0 ); // iframe: http://www.dmtcs.org/dmtcs-ojs/index.php/proceedings/article/view/dmap0106 -> http://www.dmtcs.org/dmtcs-ojs/index.php/proceedings/article/viewArticle/dmap0106
		doi.put ( "clef", 0 ); // PDF
		doi.put ( "hildesheim", 0 ); // Redirect
		doi.put ( "mulberrytech", 0 ); // Redirect
		doi.put ( "trec", 0 ); // .txt
		doi.put ( "cscan", 0 ); // PDF
		doi.put ( "siam", 0 ); // Chiant
		doi.put ( "icac", 0 ); // Chiant, big mess -> match
		doi.put ( "ceas", 0 ); // Chiant, big mess -> match
		doi.put ( "crpit", 0 ); // Dead
		doi.put ( "andrew", 0 ); // Dead
		doi.put ( "simvis", 0 ); // Dead
		doi.put ( "colt", 0 ); // Dead
		doi.put ( "uiuc", 0 ); // Dead
		doi.put ( "rwth", 0 ); // Dead
		doi.put ( "ugent", 0 ); // Dead
		doi.put ( "nbn", 0 ); // Dead
		doi.put ( "karlsruhe", 0 ); // Dead
		doi.put ( "weis2008", 0 ); // Dead
		doi.put ( "ecsi", 0 ); // Dead
		doi.put ( "gauss", 0 ); // Dead
		doi.put ( "digra", 0 ); // Dead
		doi.put ( "gamesconference", 0 ); // Dead
		doi.put ( "bmva", 0 ); // Dead
		doi.put ( "csdl", 0 ); // Dead
		doi.put ( "uniroma3", 0 ); // Dead
		doi.put ( "pitt", 0 ); // Dead
		doi.put ( "tsinghua", 0 ); // Dead
		doi.put ( "irit", 0 ); // Dead
		doi.put ( "stuba", 0 ); // Dead
		doi.put ( "ams", 0 ); // Dead
		doi.put ( "sigda", 0 ); // Dead
		doi.put ( "dead", 0 ); // Dead
		doi.put ( "others", 0 );
		while ( unmarshallerInProc.hasNext () ) {
			InProceedings ip = unmarshallerInProc.next ();
			if ( ip.getEe () != null ) {
				for ( EE ee : ip.getEe () ) {
					String str = ee.getContent ().toLowerCase ();
					if ( str != null ) {
						if ( str.endsWith ( ".pdf" ) ) {
							doi.put ( "pdf", doi.get ( "pdf" ) + 1 );

						} else if ( str.endsWith ( ".mp3" ) || str.endsWith ( ".ps" ) || str.endsWith ( ".gz" ) || str.endsWith ( ".zip" ) || str.endsWith ( ".ps.z" ) || str.endsWith ( ".ppt" ) || str.endsWith ( ".pptx" ) || str.endsWith ( ".djvu" ) ) {
							doi.put ( "strange", doi.get ( "strange" ) + 1 );

						} else if ( str.endsWith ( ".doc" ) ) {
							doi.put ( "doc", doi.get ( "doc" ) + 1 );

						} else if ( str.contains ( "jmlr.org" ) || str.contains ( "jmlr.csail.mit.edu" ) ) {
							doi.put ( "jmlr", doi.get ( "jmlr" ) + 1 );

						} else if ( str.contains ( "dx.doi.org" ) ) {
							doi.put ( "doi", doi.get ( "doi" ) + 1 );

						} else if ( str.contains ( "arxiv.org" ) ) {
							doi.put ( "arxiv", doi.get ( "arxiv" ) + 1 );

						} else if ( str.contains ( "ieeecomputersociety.org" ) ) {
							doi.put ( "ieeecomp", doi.get ( "ieeecomp" ) + 1 );

						} else if ( str.contains ( "ieeexplore.ieee.org" ) ) {
							doi.put ( "ieeexplore", doi.get ( "ieeexplore" ) + 1 );

						} else if ( str.contains ( "subs.emis.de" ) ) {
							doi.put ( "emis", doi.get ( "emis" ) + 1 );

						} else if ( str.contains ( "liris.cnrs.fr" ) ) {
							doi.put ( "cnrs", doi.get ( "cnrs" ) + 1 );

						} else if ( str.contains ( "booksonline.iospress.nl" ) ) {
							doi.put ( "iospress", doi.get ( "iospress" ) + 1 );

						} else if ( str.contains ( "doi.acm.org" ) || str.contains ( "portal.acm.org" ) || str.contains ( "dl.acm.org" ) || str.contains ( "acm.org/pubs" ) ) {
							doi.put ( "acm", doi.get ( "acm" ) + 1 );

						} else if ( str.contains ( "asso-aria.org" ) ) {
							doi.put ( "aria", doi.get ( "aria" ) + 1 );

						} else if ( str.contains ( "lrec-conf.org" ) ) {
							doi.put ( "lrec", doi.get ( "lrec" ) + 1 );

						} else if ( str.contains ( "link.springer.de" ) || str.contains ( "springerlink.com" ) ) {
							doi.put ( "springer", doi.get ( "springer" ) + 1 );

						} else if ( str.contains ( "springerlink.metapress.com" ) ) {
							doi.put ( "springerdead", doi.get ( "springerdead" ) + 1 );

						} else if ( str.contains ( "usenix.org" ) ) {
							doi.put ( "usenix", doi.get ( "usenix" ) + 1 );

						} else if ( str.contains ( "aisel.aisnet.org" ) ) {
							doi.put ( "aisnet", doi.get ( "aisnet" ) + 1 );

						} else if ( str.contains ( "aclweb.org" ) ) {
							doi.put ( "aclweb", doi.get ( "aclweb" ) + 1 );

						} else if ( str.contains ( "computer.org" ) ) {
							doi.put ( "computer", doi.get ( "computer" ) + 1 );

						} else if ( str.contains ( "ewic.bcs.org" ) || str.contains ( "www.bcs.org" ) ) {
							doi.put ( "ewic", doi.get ( "ewic" ) + 1 );

						} else if ( str.contains ( "vde-verlag.de" ) ) {
							doi.put ( "vde", doi.get ( "vde" ) + 1 );

						} else if ( str.contains ( "aaai.org" ) ) {
							doi.put ( "aaai", doi.get ( "aaai" ) + 1 );

						} else if ( str.contains ( "easychair.org" ) ) {
							doi.put ( "easychair", doi.get ( "easychair" ) + 1 );

						} else if ( str.contains ( "mensch-und-computer.de" ) ) {
							doi.put ( "mundcomp", doi.get ( "mundcomp" ) + 1 );

						} else if ( str.contains ( "espace-dev.fr" ) ) {
							doi.put ( "espacedev", doi.get ( "espacedev" ) + 1 );

						} else if ( str.contains ( "www.eg.org" ) ) {
							doi.put ( "eg", doi.get ( "eg" ) + 1 );

						} else if ( str.contains ( "knowledgecenter.siam.org" ) || str.contains ( "www.siam.org" ) ) {
							//doi.put ( "siam", doi.get ( "siam" ) + 1 );
							doi.put ( "dead", doi.get ( "dead" ) + 1 );

						} else if ( str.contains ( "drops.dagstuhl.de" ) ) {
							doi.put ( "dagstuhl", doi.get ( "dagstuhl" ) + 1 );

						} else if ( str.contains ( "www.imaging.org" ) ) {
							doi.put ( "imaging", doi.get ( "imaging" ) + 1 );

						} else if ( str.contains ( "www.zpid.de" ) ) {
							doi.put ( "zpid", doi.get ( "zpid" ) + 1 );

						} else if ( str.contains ( "weis09.infosecon.net" ) ) {
							doi.put ( "infosecon", doi.get ( "infosecon" ) + 1 );

						} else if ( str.contains ( "w3.org" ) ) {
							doi.put ( "w3", doi.get ( "w3" ) + 1 );

						} else if ( str.contains ( "nasa.gov" ) ) {
							doi.put ( "nasa", doi.get ( "nasa" ) + 1 );

						} else if ( str.contains ( "crpit.com" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "crpit", doi.get ( "crpit" ) + 1 );

						} else if ( str.contains ( "andrew.cmu.edu" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "andrew", doi.get ( "andrew" ) + 1 );

						} else if ( str.contains ( "simvis.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "simvis", doi.get ( "simvis" ) + 1 );

						} else if ( str.contains ( "colt2010.haifa.il.ibm.com" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "colt", doi.get ( "colt" ) + 1 );

						} else if ( str.contains ( "netfiles.uiuc.edu" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "uiuc", doi.get ( "uiuc" ) + 1 );

						} else if ( str.contains ( "isca-speech.org" ) ) {
							doi.put ( "isca", doi.get ( "isca" ) + 1 );

						} else if ( str.contains ( "interspeech2012.org" ) ) {
							doi.put ( "interspeech", doi.get ( "interspeech" ) + 1 );

						} else if ( str.contains ( "stanford.edu" ) ) {
							doi.put ( "stanford", doi.get ( "stanford" ) + 1 );

						} else if ( str.contains ( "informatik.rwth-aachen.de" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "rwth", doi.get ( "rwth" ) + 1 );

						} else if ( str.contains ( "geoinfo.info" ) ) {
							doi.put ( "geoinfo", doi.get ( "geoinfo" ) + 1 );

						} else if ( str.contains ( "sebd2012.dei.unipd.it" ) ) {
							doi.put ( "unipd", doi.get ( "unipd" ) + 1 );

						} else if ( str.contains ( "www2003.org" ) ) {
							doi.put ( "www2003", doi.get ( "www2003" ) + 1 );

						} else if ( str.contains ( "ippserv.ugent.be" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "ugent", doi.get ( "ugent" ) + 1 );

						} else if ( str.contains ( "decsai.ugr.es" ) ) {
							doi.put ( "ugr", doi.get ( "ugr" ) + 1 );

						} else if ( str.contains ( "trec.nist.gov" ) ) {
							doi.put ( "trec", doi.get ( "trec" ) + 1 );

						} else if ( str.contains ( "cscan.org" ) ) {
							doi.put ( "cscan", doi.get ( "cscan" ) + 1 );

						} else if ( str.contains ( "nbn-resolving.de" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "nbn", doi.get ( "nbn" ) + 1 );

						} else if ( str.contains ( "hal.archives-ouvertes.fr" ) ) {
							doi.put ( "hal", doi.get ( "hal" ) + 1 );

						} else if ( str.contains ( "ubka.uni-karlsruhe.de" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "karlsruhe", doi.get ( "karlsruhe" ) + 1 );

						} else if ( str.contains ( "ssrn.com" ) ) {
							doi.put ( "ssrn", doi.get ( "ssrn" ) + 1 );

						} else if ( str.contains ( "weis2008.econinfosec.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "weis2008", doi.get ( "weis2008" ) + 1 );

						} else if ( str.contains ( "actapress.com" ) ) {
							doi.put ( "actapress", doi.get ( "actapress" ) + 1 );

						} else if ( str.contains ( "web1.bib.uni-hildesheim.de" ) ) {
							doi.put ( "hildesheim", doi.get ( "hildesheim" ) + 1 );

						} else if ( str.contains ( "clef-initiative.eu" ) ) {
							doi.put ( "clef", doi.get ( "clef" ) + 1 );

						} else if ( str.contains ( "ecsi-association.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "ecsi", doi.get ( "ecsi" ) + 1 );

						} else if ( str.contains ( "gauss.ececs.uc.edu" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "gauss", doi.get ( "gauss" ) + 1 );

						} else if ( str.contains ( "mulberrytech.com" ) ) {
							doi.put ( "mulberrytech", doi.get ( "mulberrytech" ) + 1 );

						} else if ( str.contains ( "doc.ic.ac.uk" ) ) {
							//doi.put ( "icac", doi.get ( "icac" ) + 1 );
							doi.put ( "dead", doi.get ( "dead" ) + 1 );

						} else if ( str.contains ( "gamesconference.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "gamesconference", doi.get ( "gamesconference" ) + 1 );

						} else if ( str.contains ( "digra.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "digra", doi.get ( "digra" ) + 1 );

						} else if ( str.contains ( "dmtcs" ) ) {
							doi.put ( "dmtcs", doi.get ( "dmtcs" ) + 1 );

						} else if ( str.contains ( "stringology.org" ) ) {
							doi.put ( "stringology", doi.get ( "stringology" ) + 1 );

						} else if ( str.contains ( "ep.liu.se" ) ) {
							doi.put ( "liu", doi.get ( "liu" ) + 1 );

						} else if ( str.contains ( "bmva.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "bmva", doi.get ( "bmva" ) + 1 );

						} else if ( str.contains ( "icwsm.org" ) ) {
							doi.put ( "icwsm", doi.get ( "icwsm" ) + 1 );

						} else if ( str.contains ( "hdl.handle.net" ) ) {
							doi.put ( "hdl", doi.get ( "hdl" ) + 1 );

						} else if ( str.contains ( "bioinformatics.oxfordjournals.org" ) || str.contains ( "bioinformatics.oupjournals.org" ) ) {
							doi.put ( "bioinformatics", doi.get ( "bioinformatics" ) + 1 );

						} else if ( str.contains ( "roboticsproceedings.org" ) ) {
							doi.put ( "roboticsproceedings", doi.get ( "roboticsproceedings" ) + 1 );

						} else if ( str.contains ( "icml.cc" ) ) {
							doi.put ( "icml", doi.get ( "icml" ) + 1 );

						} else if ( str.contains ( "csdl.tamu.edu" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "csdl", doi.get ( "csdl" ) + 1 );

						} else if ( str.contains ( "dia.uniroma3.it" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "uniroma3", doi.get ( "uniroma3" ) + 1 );

						} else if ( str.contains ( "polytechnique.fr" ) ) {
							doi.put ( "polytechnique", doi.get ( "polytechnique" ) + 1 );

						} else if ( str.contains ( "uni-rostock.de" ) ) {
							doi.put ( "rostock", doi.get ( "rostock" ) + 1 );

						} else if ( str.contains ( "uai.sis.pitt.edu" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "pitt", doi.get ( "pitt" ) + 1 );

						} else if ( str.contains ( "urn.kb.se" ) ) {
							doi.put ( "urn", doi.get ( "urn" ) + 1 );

						} else if ( str.contains ( "conference.itcs.tsinghua.edu.cn" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "tsinghua", doi.get ( "tsinghua" ) + 1 );

						} else if ( str.contains ( "graphicsinterface.org" ) ) {
							doi.put ( "graphicsinterface", doi.get ( "graphicsinterface" ) + 1 );

						} else if ( str.contains ( "papers.nips.cc" ) ) {
							doi.put ( "nips", doi.get ( "nips" ) + 1 );

						} else if ( str.contains ( "irit.fr" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "irit", doi.get ( "irit" ) + 1 );

						} else if ( str.contains ( "ceas.cc" ) ) {
							//doi.put ( "ceas", doi.get ( "ceas" ) + 1 );
							doi.put ( "dead", doi.get ( "dead" ) + 1 );

						} else if ( str.contains ( "elf.stuba.sk" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "stuba", doi.get ( "stuba" ) + 1 );

						} else if ( str.contains ( "internetsociety.org" ) ) {
							doi.put ( "internetsociety", doi.get ( "internetsociety" ) + 1 );

						} else if ( str.contains ( "ams.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "ams", doi.get ( "ams" ) + 1 );

						} else if ( str.contains ( "nii.ac.jp" ) ) {
							doi.put ( "nii", doi.get ( "nii" ) + 1 );

						} else if ( str.contains ( "www.sigda.org" ) ) {
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//doi.put ( "sigda", doi.get ( "sigda" ) + 1 );

						} else if ( str.contains ( "#page=1" ) ) {
							doi.put ( "pdf", doi.get ( "pdf" ) + 1 );

						} else {
							//doi.put ( "others", doi.get ( "others" ) + 1 );
							doi.put ( "dead", doi.get ( "dead" ) + 1 );
							//System.out.println ( str );
						}
					}
				}
			}
		}
		Map<String, Integer> sorted = sortByValue ( doi );
		for ( Map.Entry<String, Integer> sort : sorted.entrySet () ) {
			System.out.println ( sort.getValue () + " ~ " + sort.getKey () );
		}
	}

}
