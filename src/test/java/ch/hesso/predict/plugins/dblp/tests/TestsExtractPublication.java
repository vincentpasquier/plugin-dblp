/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;
import ch.hesso.predict.restful.Publication;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.Language;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestsExtractPublication {

	protected static final String LANG_PROFILE = "/mnt/Damocles/master/profiles";

	@Test
	public void extractPublication () throws Exception {
		DetectorFactory.loadProfile ( LANG_PROFILE );
		FileInputStream fis = new FileInputStream ( new File ( "/home/holmes/Downloads/dblp.xml" ) );

		PartialUnmarshaller<InProceedings> unmarshallerInProc =
				new PartialUnmarshaller<> ( fis, InProceedings.class );

		int block = 100;
		while ( unmarshallerInProc.hasNext () && block > 0 ) {
			InProceedings ip = unmarshallerInProc.next ();
			for (; ; ) {
				Publication publication = new Publication ();
				String key = ip.getKey ();
				publication.setKey ( key );
				if ( !DBLPUtils.isConference ( key ) ) {
					break;
				}

				if ( !DBLPUtils.hasVenue ( ip.getCrossRef () ) ) {
					break;
				}

				String venueKey = DBLPUtils.extractVenueKey ( ip.getCrossRef () );

				if ( !DBLPUtils.isYearExtractable ( ip.getYear (), 1900, 2013 ) ) {
					System.out.println ( ip.getYear () );
					break;
				}
				publication.setYear ( DBLPUtils.extractYear ( ip.getYear (), 1900, 2013 ) );

				if ( !DBLPUtils.fillUrlDoi ( publication, ip ) ) {
					break;
				}

				if ( !DBLPUtils.fillTitleBookTitle ( publication, ip ) ) {
					System.out.println ( "no title" );
					break;
				}

				String title = publication.getTitle ();
				List<Language> languages = DBLPUtils.detectTitleLanguage ( title );
				if ( !DBLPUtils.containsLanguageAboveThreshold ( languages, "en", 0.5 ) && title.length () > 25 ) {
					System.out.println ( title );
					System.out.println (languages);
					block--;
					break;
				}

				DBLPUtils.fillPublisher ( publication, ip.getPublisher () );
				DBLPUtils.fillPages ( publication, ip.getPages () );
				DBLPUtils.fillEditor ( publication, ip.getEditor () );

				//System.out.println ( publication );
				break;
			}
		}
	}

}
