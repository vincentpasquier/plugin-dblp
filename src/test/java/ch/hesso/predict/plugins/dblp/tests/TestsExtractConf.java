/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import ch.hesso.predict.plugins.dblp.binding.CrossRef;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.plugins.dblp.binding.Year;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestsExtractConf {

	@Test
	public void extractConfs () throws Exception {
		FileInputStream fis = new FileInputStream ( new File ( "/mnt/Damocles/master/dblp/dblp.xml" ) );

		PartialUnmarshaller<InProceedings> unmarshallerInProc = new PartialUnmarshaller<> ( fis, InProceedings.class );
		Set<String> confs = new HashSet<> ();
		Set<String> venues = new HashSet<> ();
		Pattern pattern = Pattern.compile ( "(([A-z0-9]*/([A-z0-9\\-\\+]*))/[A-z0-9\\-\\+]*)" );
		Matcher matcher;
		int yearMin = 1990;
		int yearMax = 2014;
		while ( unmarshallerInProc.hasNext () ) {
			InProceedings ip = unmarshallerInProc.next ();

			String key = ip.getKey ();
			List<CrossRef> crossRefs = ip.getCrossRef ();
			if ( crossRefs == null || crossRefs.isEmpty () ) {
				continue;
			}

			List<Year> year = ip.getYear ();
			if ( year != null && !year.isEmpty () ) {
				String yearText = year.get ( 0 ).getContent ();
				if ( yearText != null ) {
					int ipYear = Integer.parseInt ( yearText );
					if ( ipYear < yearMin || ipYear > yearMax ) {
						continue;
					}
				}
			}
			CrossRef crossRef = crossRefs.get ( 0 );
			if ( crossRef == null ) {
				continue;
			}
			String strCrossRef = crossRef.getContent ();

			matcher = pattern.matcher ( key );
			if ( matcher.find () ) {
				String potentialConference = matcher.group ( 2 );
				if ( potentialConference.startsWith ( "conf" ) ) {
					String group = matcher.group ( 3 );
					confs.add ( group );
				}
			}
			matcher = pattern.matcher ( strCrossRef );
			if ( matcher.find () ) {
				String group = matcher.group ( 1 );
				venues.add ( group );
			}
		}

		int count = 0;

		for ( String conf : confs ) {
				System.out.println ( conf );
				count += 1;
		}

		System.out.println ( count );
		count = 0;

		for ( String venue : venues ) {
			if ( !venue.startsWith ( "conf" ) ) {
			} else {
				count += 1;
				System.out.println ( venue );
			}
		}

		System.out.println ( count );
	}

}
