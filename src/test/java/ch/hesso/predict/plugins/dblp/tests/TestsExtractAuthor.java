/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import ch.hesso.predict.plugins.dblp.DBLPUtils;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestsExtractAuthor {

	private final Multimap<String, String> authorNameMappingPublicationKey =
			HashMultimap.create ();

	@Test
	public void testExtractAuthor () throws Exception {
		FileInputStream fis = new FileInputStream ( new File ( "/home/holmes/Downloads/dblp.xml" ) );

		PartialUnmarshaller<InProceedings> unmarshallerInProc =
				new PartialUnmarshaller<> ( fis, InProceedings.class );

		while ( unmarshallerInProc.hasNext () ) {
			InProceedings ip = unmarshallerInProc.next ();
			String publicationKey = ip.getKey ();
			if ( DBLPUtils.hasAuthors ( ip.getAuthor () ) ) {
				Set<String> peopleName = DBLPUtils.extractAuthorsName ( ip.getAuthor () );
				for ( String personName : peopleName ) {
					authorNameMappingPublicationKey.put ( personName, publicationKey );
				}
			}
		}

		System.out.println (authorNameMappingPublicationKey);
	}

}
