/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import ch.hesso.predict.plugins.dblp.binding.EE;
import ch.hesso.predict.plugins.dblp.binding.InProceedings;
import ch.hesso.predict.plugins.dblp.unmarshall.PartialUnmarshaller;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DBLPDOIExtract {

	private static Set<String> DOIS = new HashSet<> ();

	public static void main ( String[] args ) throws IOException, JAXBException, XMLStreamException {
		int year = Integer.parseInt ( args[ 1 ] );
		extractDOIS ( args[ 0 ], year );
		/*FileOutputStream fos = new FileOutputStream ( new File ( args[ 2 ] + "_" + year + ".txt" ) );
		BufferedWriter writer = new BufferedWriter ( new OutputStreamWriter ( fos ) );
		for ( String doi : DOIS ) {
			writer.write ( doi );
			writer.newLine ();
		}
		writer.flush ();
		writer.close ();
		fos.close ();*/
	}

	private static void extractDOIS ( final String path, final int specificYear ) throws FileNotFoundException, JAXBException, XMLStreamException {
		//FileInputStream fis = new FileInputStream ( new File ( path ) );
		FileInputStream fis = new FileInputStream ( new File ( "/mnt/Damocles/master/dblp/dblp.xml" ) );
		PartialUnmarshaller<InProceedings> unmarshallerInProc
				= new PartialUnmarshaller<> ( fis, InProceedings.class );
		//Pattern patternIEEEXplore = Pattern.compile ( "10\\.1109/[A-z0-9\\-]*\\.[0-9]*\\.([0-9]*)" );
		Pattern patternIEEEXplore = Pattern.compile ( "10\\.1145/([0-9]*\\.[0-9]*)" );
		Pattern patternSpringer = Pattern.compile ( "10\\.1007/([0-9\\-_]{1,30})" );
		int block = 100;
		while ( unmarshallerInProc.hasNext () && block > 0 ) {
			try {
				InProceedings ip = unmarshallerInProc.next ();
				for (; ; ) {
					boolean extract = false;
					extract = ip.getEe () != null && ip.getEe ().size () > 0;
					if ( !extract ) {
						break;
					}

					EE ee = ip.getEe ().get ( 0 );
					String doi = ee.getContent ();
					extract = ee.getContent ().contains ( "doi.acm.org" ); // ieeecomputersociety.org
					//extract = ee.getContent ().contains ( "/10.1007/" ); // ieeecomputersociety.org

					if ( extract ) {
						block--;
					/*	Matcher matcher = patternSpringer.matcher ( ee.getContent () );
						System.out.print ( matcher.find () );
						System.out.print (" " + matcher.group ( 1 ));*/
						System.out.println ( ee.getContent () );
						break;
					}

					if ( !extract ) {
						break;
					}

					Matcher matcher = patternIEEEXplore.matcher ( ee.getContent () );
					if ( matcher.find () ) {
						extract = matcher.group ( 1 ).length () < 5;
					}

					if ( !extract ) {
						break;
					}

					/*extract = !doi.contains ( "/10.1007/" ) && !doi.contains ( "/10.7148/" ) && !doi.contains ( "/10.2312/" );

					if ( !extract ) {
						break;
					}*/

					extract = ip.getYear () != null && ip.getYear ().size () > 0;
					if ( !extract ) {
						break;
					}

					Integer year = Integer.parseInt ( ip.getYear ().get ( 0 ).getContent () );
					extract = year.equals ( specificYear );
					if ( extract ) {
						DOIS.add ( ee.getContent () );
					}
					break;
				}
			} catch ( Exception e ) {
				// LAWL OSEF
			}
		}
	}

}
