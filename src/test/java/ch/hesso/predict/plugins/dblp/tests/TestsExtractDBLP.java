/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.dblp.tests;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.NERClassifierCombiner;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestsExtractDBLP {

	protected static final String PATH_NER = "/mnt/Damocles/master/";

	protected static final String NER = PATH_NER + "english.all.3class.distsim.crf.ser.gz";

	//@Test
	public void testExtract () throws IOException {
		Document doc =
				Jsoup.connect ( "http://www.informatik.uni-trier.de/~ley/db/conf/cata/index.html" )
						.get ();
		AbstractSequenceClassifier classifier = NERClassifierCombiner.loadClassifierFromPath ( NER );
/*
		String acronym = doc.title ();
		acronym = acronym.replace ( "dblp: ", "" );

		String title = doc.select ( "h1" ).text ();
		int startAcronym = title.indexOf ( " (" + acronym + ")" );
		if ( startAcronym != -1 ) {
			title = title.substring ( 0, startAcronym );
		}

		System.out.println ( acronym );
		System.out.println ( title );
		System.out.println ( "END" );
		System.out.println ();*/
		// + URL

		// VENUES + pc member
		//Pattern patternYear = Pattern.compile ( "[A-z0-9]*/[A-z0-9\\-\\+]*/([0-9]{4}|[0-9]{2})" );
		//Matcher matcher;
		//System.out.println ( "VENUE" );
		for ( Element venues : doc.select ( ".publ-list" ).select ( "li" ) ) {
			for ( Element venue : venues.select ( "li[id^=conf]" ) ) {
				String key = venue.attr ( "id" );
				if ( key != null ) {
					// it's a venue.
					//System.out.println ( key );
					//matcher = patternYear.matcher ( key );
					//if ( matcher.find () ) {
					//	int year = Integer.parseInt ( matcher.group ( 1 ) );
					//	if ( year < 100 ) {
					//		year = 1900 + year;
					//	}
					//	System.out.println (year);
					//}
					Element data = venues.select ( "div.data" ).first ();
					String venueTitle = data.select ( "span.title" ).text ();
					System.out.println ( venueTitle );

					List<String> locations = new ArrayList<> ();
					// Cast done knowingly, do not panic !
					List<List<CoreLabel>> allLabels = classifier.classify ( venueTitle );
					for ( List<CoreLabel> labels : allLabels ) {
						for ( CoreLabel label : labels ) {
							if ( label.get ( CoreAnnotations.AnswerAnnotation.class ).equals ( "LOCATION" ) ) {
								locations.add ( label.word () );
							}
						}
					}
					System.out.println ( locations );

					System.out.println ( "PC: " );
					Elements pcMembers = data.select ( "a[href^=http://www.informatik.uni-trier.de/~ley/pers]" );
					for ( Element element : pcMembers ) {
						String name = element.text ();
						name = Normalizer.normalize ( name, Normalizer.Form.NFD );
						name = name.replaceAll ( "\\p{M}", "" );
						System.out.println ( name );
					}

					System.out.println ();
					System.out.println ();
				}
			}
		}
		System.out.println ( "VENUE" );
	}

	@Test
	public void testExtractProceedingsCommittee () throws IOException {
		Set<String> names = new HashSet<> ();
		Document doc = Jsoup
				.connect ( "http://www.informatik.uni-trier.de/~ley/db/conf/cata/index.html" )
				.get ();
		for ( Element venues : doc.select ( ".publ-list" ).select ( "li" ) ) {
			for ( Element venue : venues.select ( "li[id^=conf]" ) ) {
				String key = venue.attr ( "id" );
				if ( key != null ) {
					Element data = venues.select ( "div.data" ).first ();
					Elements proceedingsCommittees =
							data.select ( "a[href^=http://www.informatik.uni-trier.de/~ley/pers]" );
					for ( Element element : proceedingsCommittees ) {
						String name = element.text ();
						name = Normalizer.normalize ( name, Normalizer.Form.NFD );
						name = name.replaceAll ( "\\p{M}", "" );
						names.add ( name );
					}
				}
			}
		}
		System.out.println ( names );
	}

}
